package com.francescomaisto.signalstesting 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import org.osflash.signals.DeluxeSignal;
	import org.osflash.signals.events.GenericEvent;
	import org.osflash.signals.events.IBubbleEventHandler;
	import org.osflash.signals.events.IEvent;
	
	/**
	 * ...
	 * @author Francesco Maisto
	 */
	public class GreenButton extends Sprite implements IBubbleEventHandler
	{
		private var yellowButton:YellowButton;
		public var clicked:DeluxeSignal;
		private var _stica:String = "Sti Cazzi";		
		
		public function GreenButton() 
		{
			graphics.beginFill(0x00A800);
			graphics.drawCircle(200, 130, 50);
			graphics.endFill();	
			
			clicked = new DeluxeSignal(this);
			
			addEventListener(MouseEvent.CLICK, onClick);
			
			yellowButton = new YellowButton();
			addChild(yellowButton);
		}
		
		/* INTERFACE org.osflash.signals.events.IBubbleEventHandler */
		
		public function onEventBubbled(event:IEvent):Boolean 
		{
			trace ("The Green Button caught the bubbling of the Yellow Button");
			return true;
		}
		
		private function onClick(e:MouseEvent):void 
		{
			//clicked.dispatch(new GenericEvent);
		}
		
		public function get stiCazzi():String { return _stica; }
		
	}

}