package com.francescomaisto.signalstesting
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import org.osflash.signals.events.GenericEvent;
	import org.osflash.signals.events.IBubbleEventHandler;
	import org.osflash.signals.events.IEvent;
	
	/**
	 * ...
	 * @author Francesco Maisto
	 */
	public class Main extends Sprite implements IBubbleEventHandler
	{		
		private var redButton:RedButton;
		private var greenButton:GreenButton;
		private var blueButton:BlueButton;
		
		public function Main():void 
		{
			redButton = new RedButton();
			addChild(redButton);
			redButton.clicked.add(onRedButtonClick);
			
			greenButton = new GreenButton();
			addChild(greenButton);
			greenButton.clicked.add(onGreenButtonClick);
			
			blueButton = new BlueButton();
			addChild(blueButton);
			blueButton.clicked.add(onBlueButtonClick);
			blueButton.clicked.add(traceMouseX);
			
			// If you want to remove a specific listener:
			//blueButton.clicked.remove(onBlueButtonClick);
			
			// If you want to remove all listeners:
			//blueButton.clicked.removeAll();
		}
		// ------------------------------------------------------------------------------------------------------------------------------------------------
		private function onRedButtonClick():void 
		{
			trace("you clicked the red button");
		}
		// ------------------------------------------------------------------------------------------------------------------------------------------------		
		private function onGreenButtonClick(event:GenericEvent):void 
		{
			trace("you clicked on the green button");
			
			// We trace a property of the target
			trace(event.target.stiCazzi);
			
			// We can even trace the signal itself
			trace(event.signal);
		}
		// ------------------------------------------------------------------------------------------------------------------------------------------------		
		/* INTERFACE org.osflash.signals.events.IBubbleEventHandler */
		
		public function onEventBubbled(event:IEvent):Boolean 
		{
			trace("The Main class caught the bubbling coming from Yellow Button");
			return false;
		}
		// ------------------------------------------------------------------------------------------------------------------------------------------------		
		private function onBlueButtonClick(event:MouseEvent):void 
		{			
			trace("You clicked on the Blue Button!");
		}
		private function traceMouseX(event:MouseEvent):void 
		{
			// We trace a property of the target
			trace("The X of this button = " + event.localX);
		}
	}	
}