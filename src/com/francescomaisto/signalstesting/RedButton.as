package com.francescomaisto.signalstesting 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import org.osflash.signals.Signal;
	
	/**
	 * ...
	 * @author Francesco Maisto
	 */
	public class RedButton extends Sprite 
	{
		public var clicked:Signal;
		
		public function RedButton() 
		{
			graphics.beginFill(0xF53429);
			graphics.drawCircle(70, 130, 50);
			graphics.endFill();
			
			addEventListener(MouseEvent.CLICK, onClick);
			
			clicked = new Signal();
			
		}
		
		private function onClick(e:MouseEvent):void 
		{
			clicked.dispatch();
		}
		
	}

}