package com.francescomaisto.signalstesting 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import org.osflash.signals.DeluxeSignal;
	import org.osflash.signals.events.GenericEvent;
	
	/**
	 * ...
	 * @author Francesco Maisto
	 */
	public class YellowButton extends Sprite 
	{
		public var clicked:DeluxeSignal;
		
		public function YellowButton() 
		{
			graphics.beginFill(0xFDC502);
			graphics.drawRect(180, 200, 40, 40);
			graphics.endFill();
			
			addEventListener(MouseEvent.CLICK, onClick);
			
			clicked = new DeluxeSignal(this);
		}
		
		private function onClick(e:MouseEvent):void 
		{
			clicked.dispatch(new GenericEvent(true));
		}
		
	}

}