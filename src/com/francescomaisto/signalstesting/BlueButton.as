package com.francescomaisto.signalstesting 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import org.osflash.signals.natives.NativeSignal;
	
	/**
	 * ...
	 * @author Francesco Maisto
	 */
	public class BlueButton extends Sprite 
	{
		public var clicked:NativeSignal;
		
		public function BlueButton() 
		{
			graphics.beginFill(0x135FEC);
			graphics.drawCircle(330, 130, 50);
			graphics.endFill();
			
			clicked = new NativeSignal(this, MouseEvent.CLICK);
		}
		
	}

}